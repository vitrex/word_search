#!/usr/bin/python3
# -*- coding: utf-8 -*-
import argparse
import re
from fuzzywuzzy import fuzz
import logging

logging.basicConfig(format='%(asctime)s %(message)s', filename='log.log', level=logging.DEBUG)
parser = argparse.ArgumentParser(description='Enter search keyword.')
parser.add_argument('keyword', metavar='keyword', type=str,
                    help='Word to look for.')
parser.add_argument('file', metavar='file', type=str,
                    help='Word to look for.')
parser.add_argument("-l","--lv", help="Enables Levenshtein distance algorithm to perform search.",
                    action="store_true")
parser.add_argument("-t", "--top", type=int, default=5, choices=range(5, 21))
args = parser.parse_args()


def soundex_similarity_meter(a, b):
    """ Soundex scoring algorithm
    :param
        a: alphabetical string
        b: alphabetical string
    :return
        Ratio - Numeric difference between two soundex codes
        False - When a and b first letters don't match
    """

    if a[0:1] == b[0:1]:
        ratio = int(a[1:4]) - int(b[1:4])
        if ratio > 0:
            return ratio / 10
    return False

def parse_text(text):
    """Text paraser

    :param text: alphanumerical string
    :return: list of alphabetical strings split on space
    """
    refined_text = re.sub("[^a-zA-Z]+", " ", text)
    parsed_text = re.split(" ", refined_text)
    return list(set(parsed_text))


def sanitize_keyword(keyword):
    """
    :param keyword: Alphanumerical string
    :return: Alphabetical string
    """
    sanitized = re.sub(" |[^a-zA-Z]+", "", keyword)
    return str(sanitized)


def get_soundex_code_by_string(s):
    """
    :param s: alphabetical letter
    :return: soundex code of letter
    """
    s = s.upper()
    possible_values = {"BFPV": 1, "CGJKQSXZ": 2, "DT": 3, "L": 4, "MN": 5, "R": 6}

    for value_key in possible_values.keys():
        if s in value_key:
            return str(possible_values[value_key])


def soundex(string):
    """encode string to soundex code
    https://en.wikipedia.org/wiki/Soundex

    :param string: alphabetical word
    :return: Soundex code string
    """

    string = string.upper()

    soundex_code = ""
    soundex_code += string[0]

    first_letter_soundex = get_soundex_code_by_string(soundex_code)

    """ Define dictionary with all changing values.
     As for A,E,I,O,U,H,W,Y letters, change them to . easier to remove them later."""

    possible_values = {"BFPV": 1, "CGJKQSXZ": 2, "DT": 3, "L": 4, "MN": 5, "R": 6, "AEIOU": "."}
    special_chars = ["H", "W"]

    for i, letter in enumerate(string[1:]):

        if letter in special_chars:
            letter = string[i + 1]

        for value_key in possible_values.keys():

            if letter in value_key:
                prep_code = str(possible_values[value_key])

                if prep_code != first_letter_soundex:

                    if prep_code != soundex_code[-1]:
                        soundex_code += prep_code

    soundex_code = soundex_code.replace(".", "")
    soundex_code = soundex_code[:4].ljust(4, '0')

    return soundex_code


try:
    keyword = sanitize_keyword(args.keyword)
    lst = []
    data = {}

    with open(str(args.file), "r", encoding="utf-8") as outfile:
        logging.debug("Opening file %s" % args.file)
        for line in outfile:
            parsed_file_content = parse_text(line)

            for word in parsed_file_content:
                if word not in lst:
                    if len(word) > 4:
                        if args.lv:
                            ratio = fuzz.ratio(keyword, word)
                            data[ratio] = word
                        else:
                            word_soundex = soundex(word)
                            keyword_soundex = soundex(keyword)

                            if keyword_soundex == word_soundex:
                                if keyword.lower() == word.lower():
                                    data[100] = word
                            else:
                                ratio = soundex_similarity_meter(keyword_soundex, word_soundex)
                                if ratio:
                                    data[ratio] = word
                        lst.append(word)

        sorted_d = sorted(data.items(), reverse=True)

        for items in sorted_d[:args.top]:
            print(items[1])

except FileNotFoundError as file_error:
    print("File not found, provide valid filename.")
    logging.debug(file_error)
    pass
