# Word Search

"Word search" is a CLI python application that finds similar words in your submitted text file, and scores them by similarity algorithms. You can select either use Soundex or Levenshtein Distance algorithm.

  - [Soundex-wiki] - Soundex is a phonetic algorithm for indexing names by sound, as pronounced in English.
  - [Levenshtein-wiki] - In information theory, linguistics and computer science, the Levenshtein distance is a string metric for measuring the difference between two sequences.
  
# Key features
- Unique words filter - Filters out all unique words from input file, which allows to avoid all duplicates.
- Read by line - Application is reading files line by line, when line reading is done, it goes to the garbage collector. This allows to read large size files which would exceed memory limit.
- Ability to switch between two similarity algorithms. American Soundex and Levenshtein Distance
- Customizable Amount of results returned. minimum 5 maximum 20.
- Special characters removal - Each time you run this application, it is gonna remove all special non alphabetical characters under the hood.

# Libraries used

* [fuzzywuzzy] - Fuzzy string matching like a boss. It uses Levenshtein Distance to calculate the differences between sequences in a simple-to-use package.
* [logging] - Built in Python library for loggin like a boss.
* [re] - Built in Python library for regular expressions
* [argparse] - Built in Python library for arguments passing via command prompt

# Libraries choice explained
  - FuzzyWuzzy - Ships with built-in Levenshtein Distance algorithm and allows to perform various matching which leads to the best matching results.

### Installation
Python is a cross-platform interpreted programming language. You can install it on almost all of your devices and operating systems. This application requires [Python3] to be installed on your device.

- [Windows Python Installation]
- [Linux Python Installation]
- [MacOS Python Installation]

After you have successfully installed [Python3] on your device, the next step would be to clone this repository to your device:

```sh
$ git clone https://bitbucket.org/vitrex/word_search.git
```

##### Dependencies

After cloning the repository you will have to install one library that is not built-in to Python3 and crucial for this script, [Fuzzy] and [FuzzyWuzzy]. 
To do that run following commands in your Terimal or Command Prompt if you are using windows.
```sh
$ pip install fuzzywuzzy
```
After this dependency installed, you are good to go.

### Usage
In order to run this application, you should have your input (Text) file stored at the same location as the script. This script comes with *wiki_lt.txt* file you can use as your input file. After you have set up your text file the next step is to open your Terminal or Command Prompt if you are on Windows and enter all required arguments for this script.
```sh
$ /.WordSearch.py keyword filename.txt
```
Where *keyword* is your word you are looking for and *filename.txt* your input text file. The script should return TOP 5 similar words to the one you have selected.

Adding additional arguments you can choose which algorithm to use, by default it is the American Soundex and custom similarity algorithm. Adding *-l* (short) or *--lv* argument will change script logic to use [Levenshtein-wiki] Distance algorithm which works better with more complex words and other languages.
```sh
$ /.WordSearch.py keyword filename.txt -l
$ /.WordSearch.py keyword filename.txt --lv
```
You can also change how much results are returned (default is 5) by adding another parameter *-t* (short) or *--top* an integer number between *5* and *20*. This option works with both, default Soundex and Levenshtein arguments.
```sh
$ /.WordSearch.py keyword filename.txt -l -t 10
$ /.WordSearch.py keyword filename.txt -t 10
```
### Tests
Still learning... May come in the near future.

   [fuzzywuzzy]: <https://github.com/seatgeek/fuzzywuzzy>
   [logging]: <https://docs.python.org/3/library/logging.html>
   [re]: <https://docs.python.org/3/library/re.html>
   [argparse]: <https://docs.python.org/3/howto/argparse.html>
   [repo-url]: <http://ace.ajax.org>
   [Soundex-wiki]: <https://en.wikipedia.org/wiki/Soundex>
   [Levenshtein-wiki]: <https://en.wikipedia.org/wiki/Levenshtein_distance>
   [Windows Python Installation]: <https://docs.python-guide.org/starting/install3/win/#install3-windows>
   [Linux Python Installation]: <https://docs.python-guide.org/starting/install3/linux/#install3-linux>
   [MacOS Python Installation]: <https://docs.python-guide.org/starting/install3/osx/#install3-osx>
   [Python3]: <https://docs.python-guide.org/starting/installation/>
